# Simple multi-currency for Woocommerce by Invelity

## ABOUT PLUGIN

> ## Add multiple addons to Woocommerce plugin for Wordpress

 - You can set custom fees for payment for allowed shipping countries
 - You can set currency symbols for allowed shipping countries.
 - You can set product price and sale price for each shippment country.
 - You can schedule product sale price for each shippment country.
 - You can set if coupon is allowed for shippment or billing country.
 - Display currency switcher in shop, product and cart.
 - Filter orders by product and shipping country in admin orders table.
 - Allows automatic calculate prices by daily rate for not set product prices.
 - Add widget to embed the switcher in sidebar.

## INSTALL
 - Download files from this site in zip archive and rename archive "simple-multi-currency-for-woocommerce.zip"
 - Upload "simple-multi-currency-for-woocommerce.zip" archive file to the "/wp-content/plugins/" directory and extract it.
 - Activate the plugin through the "Plugins" menu in WordPress.

## DOCUMENTATION

### General Woocommerce settings
1. Set shipping countries

![g2](/uploads/fb6c11dd380b8826131c24e2c0902030/g2.jpg)

2. Set shipping zones 

![shipping](/uploads/1f69fa7e57fe9f57e3bd018d9ad8733d/shipping.jpg)


### Plugin settings
1. Set custom fees for payment for allowed shipping countries

![1a](/uploads/91ba1b4fbb90690e282ae14409936e13/1a.jpg)

2. Set currency symbols for allowed shipping countries

![2a](/uploads/24c4735dbd4455016a8e7fd2cfb2f399/2a.jpg)

3. Set product price and sale price for each shippment country

![3a](/uploads/fc8e73f334e7137146539fc6cece1c38/3a.jpg)

![3b](/uploads/d21a5c5350cf57fdc60763aeb0155a5a/3b.jpg)

4. Schedule product sale price for each shippment country

![4a](/uploads/666c76ffd62f4f6d54455b7e0c114469/4a.jpg)

5. Set if coupon is allowed for shippment or billing country

![5a](/uploads/827df6008a1881178edf130644718459/5a.jpg)

6. Display currency switcher in shop, product and cart

![6a](/uploads/2d781d2b3ca1f6532d79a0eb07e245e3/6a.jpg)

![6b](/uploads/b191a97dad49be418fc98e7e0e737e7b/6b.jpg)

7. Rewrite deafault WooCommerce reports

![screenshot-9](/uploads/e7e3fdeb6f5072834fd19fecdcec5802/screenshot-9.jpg)

## Changelog
= 1.0.1 =
* allow automatic calculate prices by daily rate for not set product prices

= 1.0.2 =
* fix automatic calculate prices

= 1.0.3 =
* add support to use with WooCommerce Gutenberg Products Block plugin

= 1.0.4 =
* add support to calculate coupons amounts with daily rate

= 1.0.5 =
* add shortcode [smcfw_convert_price_shortcode] for recalculate custom price to custom currency
* add shortcode [smcfw_product_prices_shortcode] for display all prices in product content

= 1.0.6 =
* Fix [smcfw_product_prices_shortcode] shortcode to display default product value
* Fix ajax load price and currency

= 1.0.7 =
* Fix autodetect shippment country

= 1.0.8 =
* Fix display order currency symbol on order dislay

= 1.0.9 =
* Fix switcher functions

= 1.0.10 =
* Add widget to embed the switcher in sidebar

## Filters
[List](https://gitlab.com/Novysedlak/simple-multi-currency-for-woocommerce/wikis/Filters) of possible filters hooks.

## Actions
[List](https://gitlab.com/Novysedlak/simple-multi-currency-for-woocommerce/wikis/Actions) of possible actions hooks.

## Shortcodes
[List](https://gitlab.com/Novysedlak/simple-multi-currency-for-woocommerce/wikis/Shortcodes) of possible shortcodes.

## Plugins compatibility
[WooCommerce PDF Invoices & Packing Slips](https://wordpress.org/plugins/woocommerce-pdf-invoices-packing-slips/)

[WooCommerce Gutenberg Products Block](https://sk.wordpress.org/plugins/woo-gutenberg-products-block/)