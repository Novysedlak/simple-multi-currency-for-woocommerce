<?php
add_action( 'admin_menu', 'smcfw_settings_plugin_menu' );

function smcfw_settings_plugin_menu() {
    global $smcfw_plugin_settings_page;
    $smcfw_plugin_settings_page = add_submenu_page( 
        'options-general.php', 
        '', 
        '<span class="dashicons dashicons-admin-settings"></span>'.__('SMCFW Settings','simple-multi-currency-for-woocommerce'), 
        'manage_options', 
        'smcfw-general-settings-page', 
        'smcfw_settings_page_func' 
    );
}

function smcfw_settings_page_func(){
    global $woocommerce;
    global $smcwf_settings;
    $settings = $smcwf_settings;

    do_action('smcfw_action_settings_page_func_start');
    ?>
    <div class="smcfw-settings-wrapper">
    <div style="margin-top:1em; margin-bottom:1em;"><h3><span class="dashicons dashicons-admin-settings"></span> <?php _e('Settings'); ?></h3></div><hr>
    <div sytle="margin-top:1em; margin-bottom:2em; display:block;" class="content">
<form id="setform" action="" method="post">
<table>
	<?php do_action('smcfw_action_settings_page_form_content_start'); ?>
        <tr>
<td><?php _e('Display languages switcher','simple-multi-currency-for-woocommerce'); ?></td>
<td>
    <input name="settings[display_switcher]" vlaue="1" type="checkbox" <?php if(isset($settings['display_switcher'])){ print "checked"; } ?> >
</td>
        </tr>
       <tr>
<td><?php _e('Allow multiple product prices','simple-multi-currency-for-woocommerce'); ?></td>
<td>
    <input name="settings[allow_multiple_product_prices]" vlaue="1" type="checkbox" <?php if(isset($settings['allow_multiple_product_prices'])){ print "checked"; } ?> >
</td>
        </tr>
       <tr>
<td><?php _e('Rewrite default WooCommerce reports','simple-multi-currency-for-woocommerce'); ?></td>
<td>
    <input name="settings[rewrite_default_reports]" vlaue="1" type="checkbox" <?php if(isset($settings['rewrite_default_reports'])){ print "checked"; } ?> >
</td>
        </tr>
      <tr>
<td><?php _e('Recalculate currency rates','simple-multi-currency-for-woocommerce'); ?></td>
<td>
    <input name="settings[recalculate_currency_rates]" vlaue="1" type="checkbox" <?php if(isset($settings['recalculate_currency_rates'])){ print "checked"; } ?> >
</td>
        </tr>
        <?php if( function_exists('icl_object_id') ){ ?>
      <tr>
<td><?php _e('Enable support for WPML plugin','simple-multi-currency-for-woocommerce'); ?></td>
<td>
    <input name="settings[wpml_support]" vlaue="1" type="checkbox" <?php if(isset($settings['wpml_support'])){ print "checked"; } ?> >
</td>
        </tr>
    <?php } ?>
    <?php do_action('smcfw_action_settings_page_form_content_end'); ?>
    </table>
<input type="hidden" name="action" value="smcfwupdatesettingaction">
<input type="hidden" name="tab" value="smcfwtab">
<input type="hidden" name="security" value="<?php print wp_create_nonce( 'smcfwupdatesettingaction_nonce' ) ?>">
<div class="button-div-footer" style="margin-top:2em;">
<button class="button button-primary" id="submit-form_b" type="submit"><?php _e('Submit'); ?></button>
</div>
</form>
    </div>
    <br><br>
    <hr></div>
    <?php do_action('smcfw_action_settings_page_func_end'); 
}

add_action('smcfw_action_settings_page_func_start', function(){ $css = '<style>
    .wp-core-ui .button-success, .wp-core-ui .button-success:hover, .wp-core-ui .button-success:active, .wp-core-ui .button-success:focus {
    background: #5cb85c;
    border-color: #4cae4c;
    box-shadow: 0 1px 0 #4cae4c;
    color: #fff;
    text-decoration: none;
    text-shadow: 0 -1px 1px #4cae4c, 1px 0 1px #4cae4c, 0 1px 1px #4cae4c, -1px 0 1px #4cae4c;
}
.wp-core-ui .button-danger, .wp-core-ui .button-danger:hover, .wp-core-ui .button-danger:active, .wp-core-ui .button-danger:focus {
    background: #c9302c;
    border-color: #ac2925;
    box-shadow: 0 1px 0 #ac2925;
    color: #fff;
    text-decoration: none;
    text-shadow: 0 -1px 1px #ac2925, 1px 0 1px #ac2925, 0 1px 1px #ac2925, -1px 0 1px #ac2925;
}
.tab-hide {
    display: none;
}
#setform {
    margin-top: 1em;
}
#setform nav {
    margin-bottom: 1em;
}
#setform [type="number"] {
    width: 80px;
}
.div-tabs, .button-div-footer, .smcfw-settings-wrapper {
  /*  max-width: 800px;
    margin: auto; */
}
.button-div-footer {
    text-align: left;
}

</style>'; 
print apply_filters('smcfw_action_settings_page_func_start_css',$css);
});

add_action('admin_footer','smcfw_settings_page_footer_js',100);
function smcfw_settings_page_footer_js(){ 
global $smcfw_plugin_settings_page;
$screen = get_current_screen();
if ( $screen->id != $smcfw_plugin_settings_page ){ return; }
    ?>
    <script>
    jQuery(document).ready(function($) {
    $( "form#setform" ).on( "submit", function( event ) {
        $('#submit-form_b').html('<span style="vertical-align: text-top;" class="dashicons dashicons-backup"></span>');
        $('#submit-form_b').removeClass('button-primary');
        $('#submit-form_b').addClass('button-secondary');
        $('#submit-form_b').attr('disabled',true);
    
    event.preventDefault();
    var dataar = $( this ).serialize();
    $.ajax( {
    url: '<?php print SMCFW_AJAX_URL; ?>', type: 'POST', data: dataar,}
)
  .done(function() {
    //console.log("success");
    $('#submit-form_b').addClass('button-success');
}
)
  .fail(function() {
    //console.log("error");
    $('#submit-form_b').addClass('button-error');
}
)
  .always(function() {
    //console.log("complete");
    $('#submit-form_b').attr('disabled',false);
    $('#submit-form_b').html('<?php _e('Submit'); ?>');
}
);
    return false;
}
);
});
</script>
<?php } ?>