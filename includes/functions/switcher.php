<?php
function smcfw_init($content){
if(is_checkout()){ return; }
global $smcfw_shortcode_products;
if(is_cart() or is_shop() or is_woocommerce() or $smcfw_shortcode_products==true){
	do_action('smcfw_init_add_switcher');
}
}

add_action('wp_footer','smcfw_init');

add_action('smcfw_init_add_switcher','smcfw_init_add_switcher_callback');

function smcfw_init_add_switcher_callback(){
print apply_filters('smcfw_filter_init_add_switcher_callback_css','
<style>
	#language-selector-div{ position:fixed !important;  right:0px; top:40%; z-index:9999; padding:5px; padding-top:0px; background-color: white; }
	#language-selector-div a{ display:block;  }
	#language-selector-div a.grayscale{ filter:grayscale(100); }
	#language-selector-div a.nograyscale{ filter:grayscale(0); }
	#language-selector-div a:hover{
	-moz-transition: all .5s ease-in;
    -ms-transition: all .5s ease-in;
    -o-transition: all .5s ease-in;
    transition: all .5s ease-in;
    filter:grayscale(0);
	}
</style>');
print '<div id="language-selector-div" class="shadow">';
do_action('smcfw_display_switcher');
print '</div>';
}

add_action('smcfw_display_switcher','smcfw_create_display_switcher');
function smcfw_create_display_switcher(){
	?>
	<?php $arr=smcfw_get_allowed_countries();
	if(isset($arr)){
		if(count($arr)>1){
foreach($arr as $key=>$title){ ?>
	<a class="<?php 
$sc = smcfw_get_shipping_country();
if($sc <> ''){
	if($key==$sc){ print 'nograyscale'; } else{ print 'grayscale'; }
} else{
	if($key==smcfw_get_base_country()){print 'nograyscale'; } else{ print 'grayscale';}
}
	?>" title="<?php print $title; ?>" href="./?smcfw_change_currency=<?php print $key; ?>"><span class="shadow"><?php print smcfw_get_flag($key, $title); ?></span></a>
<?php } } } ?>

<?php
}

function smcfw_prefix_detect_shortcode()
{
	global $smcfw_shortcode_products;
    global $wp_query;	
    $posts = $wp_query->posts;
    $pattern = get_shortcode_regex();
    $smcfw_shortcode_products = false;
    if(isset($posts) and !is_admin()){
    foreach ($posts as $post){
		if (   preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches )
			&& array_key_exists( 2, $matches )
			&& in_array( 'products', $matches[2] ) )
		{
			// enque my css and js
			$smcfw_shortcode_products = true;
			break;	
		}    
    }
}
}
add_action( 'wp', 'smcfw_prefix_detect_shortcode' );