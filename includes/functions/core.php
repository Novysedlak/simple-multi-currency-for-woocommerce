<?php
function smcfw_get_current_gateway() {
		global $woocommerce;

		$available_gateways = smcfw_get_payment_gateways();
		$current_gateway = null;
		$default_gateway = get_option( 'woocommerce_default_gateway' );
		if ( ! empty( $available_gateways ) ) {

		   // Chosen Method
			if ( isset( WC()->session->chosen_payment_method ) && isset( $available_gateways[ WC()->session->chosen_payment_method ] ) ) {
				$current_gateway = $available_gateways[ WC()->session->chosen_payment_method ];
			} elseif ( isset( $available_gateways[ $default_gateway ] ) ) {
				$current_gateway = $available_gateways[ $default_gateway ];
			} else {
				$current_gateway = current( $available_gateways );
			}
		}
		if ( ! is_null( $current_gateway ) ){
			$current_gateway = $current_gateway;
		} else {
			$current_gateway = false;
		}
		return apply_filters('smcfw_filter_get_current_gateway',$current_gateway);
}

function smcfw_get_flag($lang, $lang_title = ''){
if(!isset($lang)){ return ''; }
$img = '<img style="height: 16px; width: auto;" class="smcfw-flag-icon" src="'.SMCFW_DIR_URL.'assets/flags/'.strtolower($lang).'.svg" alt="'.$lang_title.'">';
return apply_filters('smcfw_filter_get_flag',$img);
}

function smcfw_set_shipping_country_directly(){
	global $woocommerce;

	if(is_checkout() or is_ajax() or is_admin()){ return; }
	if( !isset( WC()->customer ) ){ return; }
		if(is_null(WC()->customer->get_shipping_country())){ smcfw_set_shipping_country(smcfw_get_base_country()); }
}

function smcfw_set_shipping_country($country_code){
global $woocommerce;
$code = apply_filters('smcfw_filter_set_shipping_country',$country_code);
WC()->customer->set_shipping_country($code);
return $code;
}

function smcfw_get_shipping_country(){
global $woocommerce;
//return WC()->customer->get_shipping_country();
//smcfw_set_shipping_country_directly();
if( !isset( WC()->customer ) ){ return apply_filters('smcfw_filter_country_code',smcfw_get_base_country()); }
if(null == WC()->customer->get_shipping_country()){ return apply_filters('smcfw_filter_country_code',smcfw_get_base_country()); }
$country = apply_filters('smcfw_filter_get_shipping_country',WC()->customer->get_shipping_country());
$country = strtoupper($country);
//print $country.'b'.WC()->customer->get_shipping_country();
return apply_filters('smcfw_filter_country_code',$country);
}

function smcfw_get_base_country(){
global $woocommerce;
$country = WC()->countries->get_base_country();
return apply_filters('smcfw_filter_get_base_country',$country);
}

function smcfw_get_allowed_countries(){
global $woocommerce;
$countries = WC()->countries->get_allowed_countries();
return apply_filters('smcfw_filter_get_allowed_countries',$countries);
}

function smcfw_get_payment_gateways(){
		global $woocommerce;
		$available_gateways = WC()->payment_gateways->payment_gateways();
		return apply_filters('smcfw_filter_get_payment_gateways',$available_gateways);
}

function smcfw_get_gateways(){
$arr = get_option( 'smcfw_gateways' );
return apply_filters('smcfw_filter_get_gateways',$arr);
}

function smcfw_get_curency(){
$arr = get_option( 'smcfw_currency' );
return apply_filters('smcfw_filter_get_currency',$arr);
}

function smcfw_get_freeshipping(){
$arr = get_option( 'smcfw_freeshipping' );
return apply_filters('smcfw_filter_get_freeshipping',$arr);
}

function smcfw_sanitize_array($array){
$tags = $array;
return apply_filters('smcfw_filter_sanitize_array',$tags);
}

function smcfw_get_settings(){
$arr = get_option('smcfw_settings');
return apply_filters('smcfw_filter_smcfw_settings',$arr);
}

function smcfw_get_price($currency_symbol='', $price=0){
	//$s = wc_format_decimal($price). $currency_symbol;
	global $woocommerce;
	if($currency_symbol==get_woocommerce_currency_symbol()){ return wc_price($price); }
	$s = sprintf( get_woocommerce_price_format(), $currency_symbol, $price);
	return apply_filters('smcfw_filter_get_price',$s);
}